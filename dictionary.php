<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
$db = $client->recipes;

$definitions=array();
if(!empty($_GET["letter"])) {
    //подключение к дазе данных и извлечение всех слов с началом letter
    $collection=$db->dictionary;
    $def=$collection->find();

    foreach($def as $d) {
        if(mb_strtolower($d["name"][0].$d["name"][1])
            !=mb_strtolower($_GET["letter"])) continue;

        $definitions[]=array(
                "name"=>$d["name"],
                "definition"=>$d["definition"]
        );
    }
    /*if($_GET["letter"]=="С") {
        $definitions[0]["name"]="Слово1";
        $definitions[0]["definition"]="описание описание описание 
            описание описание описание1";
        $definitions[1]["name"]="Слово2";
        $definitions[1]["definition"]="описание описание описание 
            описание описание описание2";
        $definitions[2]["name"]="Слово3";
        $definitions[2]["definition"]="описание описание описание 
            описание описание описание3";
        $definitions[3]["name"]="Слово4";
        $definitions[3]["definition"]="описание описание описание 
            описание описание описание4";
        $definitions[4]["name"]="Слово5";
        $definitions[4]["definition"]="описание описание описание 
            описание описание описание5";
    }*/
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Рецепты</title>
</head>
<style>
    body {
        background-color: black;
    }
    h2 {
        font-weight: bold;
    }
    p {
        font-size: 20px;
    }
</style>
<body>
    <header>
        <?php require "header.php"; ?>
    </header>

    <section id="list_of_letters"">
        <div class="container">
            <div class="row">
                <?php
                $alphabet=array("А","Б","В","Г","Д","Е",
                    "Ё","Ж","З","И","Й","К","Л","М","Н",
                    "О","П","Р","С","Т","У","Ф","Х","Ц",
                    "Ч","Ш","Щ","Э","Ю","Я");
                foreach($alphabet as $i) {
                    echo '<div class="col-md-1">';
                    echo '<a href="dictionary.php?letter='.$i.'">'.$i.'</a>';
                    echo '</div>';
                }
                ?>
            </div>
        </div>
    </section>

    <section id="list_of_found_definitions">
        <ul class="terms">
            <?php
            if(!empty($_GET["letter"])&empty($definitions)) {
                echo '<li>слова на эту букву отсутствуют</li>';
            }

            foreach($definitions as $i) {
                echo '<li>';
                echo '<h2>'.$i["name"].'</h2>';
                echo '<p>'.$i["definition"].'</p>';
                echo '</li>';
            }
            ?>
        </ul>
    </section>
</body>
</html>