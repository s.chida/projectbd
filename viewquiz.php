<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
$db = $client->recipes;

session_start();
$signin=true;
if(empty($_SESSION["name"])) {
    $signin=false;
} else {
	$collection=$db->quizzes;
	$quizdb=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["quiz"])));
	//$quizdb=$collection->findOne(array("name"=>"n2"));

	$quiz=array();
	$quiz["name"]=$quizdb["name"];
	$quiz["count_q"]=$quizdb["count_q"];
	$quiz["q"]=array();
	$quiz["v"]=array();
	$quiz["correct_r"]=array();
	for($i=0;$i<$quiz["count_q"];$i++) {
		$quiz["q"][$i]=$quizdb["q".($i+1)];
		$quiz["v"][$i]=array();
		
		$collection=$db->response_quizzes;
		$resdb=$collection->findOne(array("_id"=>$quizdb["r".($i+1)]));
		for($j=0;$j<$resdb["count_r"];$j++) {
			$quiz["v"][$i][$j]=$resdb["v".($j+1)];
		}
		$quiz["correct_r"][$i]=$resdb["correct_r"];
	}
	
	if(!empty($_POST["sending"])) {
		$check=true;
		
		$quiz["answer"]=array();
		for($i=0;$i<$quiz["count_q"];$i++) {
			$quiz["answer"][$i]=$_POST["r".($i+1)];
		}
	}
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="viewquiz.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Викторины</title>
</head>
<body>
    <header>
        <?php require "header.php"; ?>
    </header>

    <?php
    if($signin==false) {
		if(empty($_SESSION["name"])) {
			echo "<p>чтобы воспользоваться этой стриницей 
                необхидимо зайти как загирустрированных 
                пользователь</p></body></html>";
		}
        exit();
    }
    ?>
	
	<section>
        <form action="viewquiz.php?quiz=<?php echo $_GET["quiz"]; ?>" method="POST">
					<h2>Викторина"<?php echo $quiz["name"]; ?>"</h2>
					<ol>

						<?php

						for($i=0;$i<$quiz["count_q"];$i++) {
                            echo ' <li class="list">';
							echo '<h3>'.$quiz["q"][$i].'</h3><br/>';
							if(!empty($_POST["sending"])) echo '<p>правильный ответ есть '.$quiz["correct_r"][$i].'</p>';
							for($j=0;$j<count($quiz["v"][$i]);$j++) {
								echo '<li><input type="radio" name="r'.($i+1).'" value="'.$j.'" ';
								if(!empty($_POST["sending"])&&$_POST["r".($i+1)]==$j || empty($_POST["sending"])&&$j==0) echo ' checked ';
								echo '/><span>'.$quiz["v"][$i][$j].'</span></p></li>';
							}	echo '	</li>';

						}

						?>

                    </ol>
                    <br />
					<?php
                    if(empty($_POST["sending"])) {
						echo '<input name="sending" value="1" readonly style="display: none">';
						echo '<input class="btn" type="submit" value="отправить">';
					}
					?>
				</form>

	</section>
</body>
</html>