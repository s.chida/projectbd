<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');

$db = $client->recipes;
session_start();

//при переходе на новую категорию 1 уровня создание массива категорий и при не 1 уровне для категории добавление в массив категорий нового уровня
//при переходе в новую категорию при нахождении в листе дерева категорий переход происхлдит в категорию этого же уровня
if(!empty($_GET["category_header"])&&$_GET["category_header"]!="return_to_last_category") {
	if(empty($_SESSION["category"])||$_SESSION["category"]==array()) {
		$_SESSION["category"]=array();
		$_SESSION["category"][]=$_GET["category_header"];

        $cat='category'.(count($_SESSION["category"]));
        $collection=$db->$cat;
        $current_cat=$collection->findOne(array("pname"=>$_SESSION["category"][count($_SESSION["category"])-1]));
	} else {
		$cat='category'.(count($_SESSION["category"])+1);
		$collection=$db->$cat;
		$current_cat=$collection->findOne(array("pname"=>$_GET["category_header"]));
		if(empty($current_cat)) {
		    $this_layer=true;
            $cat='category'.(count($_SESSION["category"]));
            $collection=$db->$cat;
            $current_cat=$collection->findOne(array("pname"=>$_GET["category_header"]));
        }

		if($current_cat["subc_n"]!=0) {
            if($this_layer==true) {
                $_SESSION["category"][count($_SESSION["category"]) - 1]=$_GET["category_header"];
            } else {
                $_SESSION["category"][]=$_GET["category_header"];
            }
			//$_SESSION["category"][]=$_GET["category_header"];
		} else {
		    if($this_layer==true) {
                $_SESSION["category"][count($_SESSION["category"]) - 1] = $_GET["category_header"];
            } else {
                $_SESSION["category"][count($_SESSION["category"])] = $_GET["category_header"];
            }
		}
	}
}

if($_GET["category_header"]=="return_to_last_category") {
	array_pop($_SESSION["category"]);

    $cat='category'.(count($_SESSION["category"]));
    $collection=$db->$cat;
    $current_cat=$collection->findOne(array("pname"=>$_SESSION["category"][count($_SESSION["category"])-1]));
}

?>
<div id="block-body">
    <header>
        <div class="logo">
            <a href="index.php">
                <span class="f">FOOD FIRST</span>
            </a>
        </div>
        <div class="top-menu">
            <ul>
                <?php
					if(!empty($_SESSION["category"])&&!($_SESSION["category"])==array()) {
						echo '<li><a href="viewmenu.php?category_header=return_to_last_category">назад</a></li>';
					}
					
					//при пустоте списка с последовательностью категорий поиск всех категорий 1 уровня
					//при не пустоте списка всех категорий 
					//при нахождении в листе вывод всех категорий из этого уровня
					//при нахождении не в листе поиск всех категорий слудующего уровня
					if(empty($_SESSION["category"])||$_SESSION["category"]==array()) {
						$collection = $db->category1;
						$cursor = $collection->find();
						foreach ($cursor as $obj) {
							echo '  <li><a href="viewmenu.php?category_header='.$obj['pname'].'&cat='.$obj['name'].'">'.$obj['name'].' </a></li>';
						}
					} else if($current_cat["subc_n"]!=0) {
						$cat='category'.(count($_SESSION["category"])+1);
						$collection=$db->$cat;
						
						for ($i=1;$i<=$current_cat["subc_n"];$i++) {
							$obj=$collection->findOne(array("pname"=>$current_cat["subc".$i]));
							echo '  <li><a href="viewmenu.php?category_header='.$obj['pname'].'&cat='.$obj['name'].'">'.$obj['name'].' </a></li>';
						}
					} else {
						if(count($_SESSION["category"])==1) {
							$collection = $db->category1;
							$cursor = $collection->find();
							foreach ($cursor as $obj) {
								echo '  <li><a href="viewmenu.php?category_header='.$obj['pname'].'&cat='.$obj['name'].'">'.$obj['name'].' </a></li>';
							}
						} else {
							$cat='category'.(count($_SESSION["category"])-1);
							$collection=$db->$cat;					$collection=$db->$cat;
                            $pr_cat=$collection->findOne(array("pname"=>$_SESSION["category"][count($_SESSION["category"])-2]));
							$cat='category'.(count($_SESSION["category"]));
							$collection=$db->$cat;

                            for ($i=1;$i<=$pr_cat["subc_n"];$i++) {
								$obj=$collection->findOne(array("pname"=>$pr_cat["subc".$i]));echo '  <li><a href="viewmenu.php?category_header='.$obj['pname'].'&cat='.$obj['name'].'">'.$obj['name'].' </a></li>';
							}
						}
					}
                    ?>
				
                <li><a href="dictionary.php">Словарь</a></li>
                <?php
				if(!empty($_SESSION["name"])) echo '<li><a href="menuquiz.php">викторины</a></li>';
				?>
				</ul>
        </div>
        <div class="block-top-auth">
            <?php session_start(); ?>
            <p><a class="login" href="login.php">
                    <?php
                    if(!empty($_SESSION["name"])) echo $_SESSION["name"];
                    else echo "Войти";
                    ?>
                </a></p>
            <?php
            if(!empty($_SESSION["name"])) {
                echo '<p><a href="personal_page.php"> личный кабинет</a></p>';
            }
            ?>

        </div>
    </header>
</div>