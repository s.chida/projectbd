<?php

require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/mydb?retryWrites=true&w=majority');

$db = $client->mydb;

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <title>Рецепты</title>
    </head>
    <?php  include("header.php"); ?>
<body>
<img src="TASTY.jpg" width="100%">
<div class="block1">
<div id="about">
    <h1>Большая книга рецептов</h1>
    <p style="text-align: center; color: wheat; font-size: 20px;">
        На нашем сайте Вы можете искать рецепты, выбирая категорию блюда, оставлять отзывы на рецепты,
        а также пользоваться словарем кулинарных терминов и добавлять собственные рецепты<br>
        <a href="addrecipe.php" id="btn" style="margin: 40px;">Добавить рецепт</a>
    </p>
</div></div>
<footer>
    <div id="container">
        <h2 style="text-align: left; color: mediumseagreen">Обратная связь</h2>
        <div style="text-align: left; color: wheat">E-mail: projectbd215@gmail.com</div>
        <div style="text-align: left; color: wheat">© ООО «FOOD-FIRST.COM», 2021.</div>
        <div>
            <h2 style="text-align: left; color: mediumseagreen">Наши соцсети</h2>
            <ul class="social-icons">
                <li><a class="social-icon-vk" href="#" target="_blank" rel="noopener"></a></li>
                <li><a class="social-icon-telegram" href="#" target="_blank" rel="noopener"></a></li>
                <li><a class="social-icon-inst" href="#" target="_blank" rel="noopener"></a></li>
            </ul>
        </div>
    </div>
</footer>
</body>
</html>

