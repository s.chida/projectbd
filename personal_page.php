<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
$db = $client->recipes;

session_start();
$signin=true;
if(empty($_SESSION["name"])) $signin=false;
else {
    if(!empty($_POST["sending"])&&!empty($_POST["category"])) {
        if(empty($_POST["categories_pname"])) {
            $collection="category1";
            $collection=$db->$collection;

            $category=array();
            $category["pname"]=$_POST["pname"];
            $category["name"]=$_POST["name"];
            $category["sunc_n"]=0;
            $collection->insertOne($category);

            $_POST=false;
            header('location:index.php');
        }
        $collection="category".(count($_POST["categories_pname"]));
        $collection=$db->$collection;

        $categorydb=$collection->findOne(array("pname"=>$_POST["categories_pname"][count($_POST["categories_pname"])-1]));
        $category["pname"]=$categorydb["pname"];
        $category["name"]=$categorydb["name"];
        $category["subc_n"]=$categorydb["subc_n"]+1;
        for($i=1;$i<=$categorydb["subc_n"];$i++) {
            $category["subc".$i]=$categorydb["subc".$i];
        }
        $category["subc".$i]=$_POST["pname"];
        //$category["subc_n"]++;
        //$category["subc".$category["subc_n"]]=$_POST["pname"];
        $collection->deleteOne(array("pname"=>$category["pname"]));
        $collection->insertOne($category);

        $collection="category".(count($_POST["categories_pname"])+1);
        $collection=$db->$collection;

        $category=array();
        $category["pname"]=$_POST["pname"];
        $category["name"]=$_POST["name"];
        $category["sunc_n"]=0;
        $collection->insertOne($category);

        $_POST=false;
        header('location:index.php');
        //$category_name=$collection->findOne(array("name"=>$_POST["name"]));
        //$category_pname=$collection->findOne(array("pname"=>$_POST["pname"]));

        //if(empty($category_name)&&empty($category_pname)) {
        //	$category=array();
        //	$category["name"]=$_POST["name"];
        //	$category["pname"]=$_POST["pname"];
        //	$category["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        //	$collection->insertOne($category);
        //}
    }
    if(!empty($_POST["sending"])&&!empty($_POST["dictionary"])) {
        $collection=$db->dictionary;

        $collection->insertOne(array("name"=>$_POST["name"],"definition"=>$_POST["definition"]));

        $_POST=false;
        header('location:index.php');
    }

    //подключение к базе данных и получение всех рецетов
    //с {user: {db.user.find{name: "$_SESSION["name"]"}}}
    //для кажной категории

    if(!empty($_SESSION["admin"])) {
        $collection=$db->changed_recipes;
        $recipes=$collection->find();
        $changed_recipe_admin=array();
        $i=0;
        foreach($recipes as $dbrecipe) {
            $changed_recipe_admin[$i]=array();

            $changed_recipe_admin[$i]["_id"]=$dbrecipe["_id"];
            $changed_recipe_admin[$i]["category"]=$dbrecipe["category1"];
            $changed_recipe_admin[$i]["name"]=$dbrecipe["name"];
            $changed_recipe_admin[$i]["ing"]=array();
            $changed_recipe_admin[$i]["c"]=array();
            for($i=1;$i<=$dbrecipe["count_ing"];$i++) {
                $changed_recipe_admin[$i]["ing"][]=$dbrecipe["ing".$i];
                $changed_recipe_admin[$i]["c"][]=$dbrecipe["c".$i];
            }
            $changed_recipe_admin[$i]["description"]=$dbrecipe["description"];
            $changed_recipe_admin[$i]["ldescription"]=$dbrecipe["ldescription"];

            $i++;
        }
    }



    $categories=array();
    function build_catefories(&$categories) {
        $c=array();
        $client = new MongoDB\Client(
            'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
        $db = $client->recipes;

        $collection=$db->category1;
        foreach($collection->find() as $r) {
            $c[0]=$r;
            $categories[]=build_categories_pr($c);
        }
    }
    function build_categories_pr(&$c) {
        $categories=array();
        $client = new MongoDB\Client(
            'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
        $db = $client->recipes;
        $categories["name"]=$c[count($c)-1]["name"];
        $categories["pname"]=$c[count($c)-1]["pname"];

        if($c[count($c)-1]["subc_n"]==0) return $categories;

        $cat="category".(count($c)+1);
        $collection=$db->$cat;

        $categories["subc_n"]=$c[count($c)-1]["subc_n"];
        $categories["subc"]=array();
        for($i=1;$i<=$c[count($c)-1]["subc_n"];$i++) {
            $r=$collection->findOne(array("pname"=>$c[count($c)-1]["subc".$i]));
            $c[count($c)]=$r;

            $categories["subc"][]=build_categories_pr($c);

            array_pop($c);
        }

        return $categories;
    }
    build_catefories($categories);
    //print_r($categories);

    //print the three of the categories for javascript
    function print_categories(&$categories) {
        echo 'let categories=';
        print_categories_pr($categories);
        echo ';';
    }
    function print_categories_pr($categories) {
        echo '[';
        for($i=0;$i<count($categories);$i++) {
            echo '{';
            echo 'name:"'.$categories[$i]["name"].'",';
            echo 'pname:"'.$categories[$i]["pname"].'"';
            if(!empty($categories[$i]["subc"])&&count($categories[$i]["subc"])!=0) {
                echo ',';
                echo 'subc_n:'.count($categories[$i]["subc"]).',';
                echo 'subc:';
                print_categories_pr($categories[$i]["subc"]);
            }
            echo '}';
            if(count($categories)-1!=$i) echo ',';
        }
        echo ']';
    }
    //$collection=$db->category;
    //$categories=$collection->find();



    $accepted_recipes=array();
    foreach($categories as $c) {
        $accepted_recipes[$c["pname"]]=array();

        $collection='recipe'.$c["pname"];
        $collection=$db->$collection;
        $recipes=$collection->find();
        $i=0;
        foreach($recipes as $dbrecipe) {
            $accepted_recipes[$c["pname"]][$i]=array();

            $accepted_recipes[$c["pname"]][$i]["category_pname"]=$c["pname"];
            $accepted_recipes[$c["pname"]][$i]["category"]=$dbrecipe["category"];
            $accepted_recipes[$c["pname"]][$i]["name"]=$dbrecipe["name"];
            $accepted_recipes[$c["pname"]][$i]["ing"]=array();
            $accepted_recipes[$c["pname"]][$i]["c"]=array();
            for($i=1;$i<=$dbrecipe["count_ing"];$i++) {
                $accepted_recipes[$c["pname"]][$i]["ing"][]=$dbrecipe["ing".$i];
                $accepted_recipes[$c["pname"]][$i]["c"][]=$dbrecipe["c".$i];
            }
            $accepted_recipes[$c["pname"]][$i]["description"]=$dbrecipe["description"];
            $accepted_recipes[$c["pname"]][$i]["ldescription"]=$dbrecipe["ldescription"];

            $i++;
        }
    }

    $collection=$db->user;
    $user_id=$collection->findOne(array("name"=>$_SESSION["name"]));
    $user_id=$user_id["_id"];

    $collection=$db->banned_recipes;
    $recipes=$collection->find(array("user_id"=>new MongoDB\BSON\ObjectId($user_id)));
    $banned_recipes=array();
    $i=0;
    foreach($recipes as $dbrecipe) {
        $banned_recipes[$i]=array();

        //$banned_recipes[$i]["category_pname"]=$c["pname"];
        $banned_recipes[$i]["_id"]=$dbrecipe["_id"];
        $banned_recipes[$i]["category"]=$dbrecipe["category1"];
        $banned_recipes[$i]["name"]=$dbrecipe["name"];
        $banned_recipes[$i]["ing"]=array();
        $banned_recipes[$i]["c"]=array();
        for($j=1;$j<=$dbrecipe["count_ing"];$j++) {
            $banned_recipes[$i]["ing"][]=$dbrecipe["ing".$j];
            $banned_recipes[$i]["c"][]=$dbrecipe["c".$j];
        }
        $banned_recipes[$i]["description"]=$dbrecipe["description"];
        $banned_recipes[$i]["ldescription"]=$dbrecipe["ldescription"];
        $banned_recipes[$i]["message"]=$dbrecipe["message"];

        $i++;
    }

    $collection=$db->changed_recipes;
    $recipes=$collection->find(array("user_id"=>new MongoDB\BSON\ObjectId($user_id)));
    $changed_recipes=array();
    $i=0;
    foreach($recipes as $dbrecipe) {
        $changed_recipes[$i]=array();

        //$changed_recipes[$i]["category_pname"]=$c["pname"];
        $changed_recipes[$i]["_id"]=$dbrecipe["_id"];
        $changed_recipes[$i]["category"]=$dbrecipe["category1"];
        $changed_recipes[$i]["name"]=$dbrecipe["name"];
        $changed_recipes[$i]["ing"]=array();
        $changed_recipes[$i]["c"]=array();
        for($j=1;$j<=$dbrecipe["count_ing"];$j++) {
            $changed_recipes[$i]["ing"][]=$dbrecipe["ing".$j];
            $changed_recipes[$i]["c"][]=$dbrecipe["c".$j];
        }
        $changed_recipes[$i]["description"]=$dbrecipe["description"];
        $changed_recipes[$i]["ldescription"]=$dbrecipe["ldescription"];

        $i++;
    }
    /*$recipes=array();
    $recipes[0]=array(
        "name"=>"рецепт1",
        "description"=>"описание создания рецепта описание создания рецепта1"
    );
    $recipes[1]=array(
        "name"=>"рецепт2",
        "description"=>"описание создания рецепта описание создания рецепта 2"
    );
    $recipes[2]=array(
        "name"=>"рецепт3",
        "description"=>"описание создания рецепта описание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецептаописание создания рецепта
        описание создания рецепта
        описание создания рецепта 3"
    );*/
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="personal_page.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Личный кабинет</title>

    <?php
    if(!empty($_SESSION["admin"])) {
        echo '
	<script>';
        print_categories($categories);
        echo '
        function change_categories(n) {
            //let a=document.getElementsByTagName("p")[0];
            //a.innerHTML=a.innerHTML+n;

            let selects=choosecategory.getElementsByTagName("select");
            let l=selects.length;
            for(let i=n+1;i<=l;i++) {
                selects[n].remove();
            }

            let cat=JSON.parse(JSON.stringify(categories));
            for(let i=0;i<selects.length;i++) {
                let j;
                for(j=0;j<cat.length;j++) if(cat[j].pname==selects[i].value) break;
                cat=cat[j].subc;
            }

            while(cat!=undefined) {
                let newop=document.createElement("select");
                newop.name=\'categories_pname[\'+n+\']\';
                let m=n+1;
                newop.onchange=function() { change_categories(m); };
                newop.required=true;
                let newopHTML="";
                for(let i=0;i<cat.length;i++) {
                    newopHTML=newopHTML+\'<option value="\'+cat[i][\'pname\']+\'">\'+cat[i][\'name\']+\'</option>\n\';
                }
                newop.innerHTML=newopHTML;

                choosecategory.append(newop);

                n++;
                cat=cat[0].subc;
            }
            //console.log(choosecategory.innerHTML);
        }
		
		function delete_last_category() {
			let selects=choosecategory.getElementsByTagName("select");
            selects[selects.length-1].remove();
		}
    </script>';
    }
    ?>
</head>
<body>
<header>
    <?php require "header.php"; ?>
</header>

<?php
if($signin==false) {
    echo '<p>чтобы воспользоваться этой страницей 
            необходимо войти как зарегистрированный пользователь</p>';
    echo '</body></html>';
    exit();
}
?>

<?php
if(!empty($_SESSION["admin"])) {
    echo '

		<section id="admin_check_recipes">
		<h2>Изменённые рецепты для проверки</h2>
		<ul>';

    if(empty($changed_recipe_admin) ) echo '<li>отстутствуют</li>';
    else {
        echo '<li class="recipe_list">';
        echo '<div class="elem_recipe">';
        foreach ($changed_recipe_admin as $recipe) {

            echo  '  <a href="viewrecipe.php?recipe='.$recipe['_id'].'&check=1&namerecipe='.$recipe['name'].'">'.$recipe['name'].' </a>';


        }echo '</div>';
        echo '</li>';
    }
    echo '
		</ul>
		</section>
		
		<section id="admin_add_category">
		<h2>Добавить категорию</h2>
		<form action="personal_page.php" method="POST" id="form_for_category">
			<div id="choosecategory">
				<select name="categories_pname[0]" onchange="change_categories(1)">
					<option value="pasta">паста</option>
					<option value="desert">десерт</option>
					<option value="other">другое</option>
				</select>
			</div>
			<button type="button" onclick="delete_last_category()">удалить последнюю категорию</button>
			<script>change_categories(0);</script>
			<br/>
			<input class="field" required name="name" placeholder="название для пользователя" />
			<input class="field" required name="pname" placeholder="название для системы" /><br/>
			
           <input   name="sending" value="1" readonly style="display: none">
            <input name="category" value="1" readonly style="display: none">
            <input class="btn" type="submit" value="отправить">
		</form>
		<br/>
		<h2>доступные категории</h2>
		<ul class="border">
		';
    /*$collection=$db->category;
    $categories=$collection->find();
    foreach($categories as $c) {
        echo '<li>'.$c["name"].' ('.$c["pname"].')</li>';
    }*/
    function print_avaible_categories($n,$pname) {
        $client = new MongoDB\Client(
            'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
        $db = $client->recipes;
        $collection='category'.($n);
        $collection=$db->$collection;

        if($n==1) {
            foreach($collection->find() as $c) {
                echo '<li>';

                echo $c["name"]."(".$c["pname"].")";
                print_avaible_categories(2,$c["pname"]);

                echo '</li>';
            }
        } else {
            $collection='category'.($n-1);
            $collection=$db->$collection;

            $last=$collection->findOne(array("pname"=>$pname));
            if($last['subc_n']==0) return;

            $collection='category'.($n);
            $collection=$db->$collection;
            echo '<ul>';
            for($i=1;$i<=$last["subc_n"];$i++) {
                echo '<li>';

                $c=$collection->findOne(array("pname"=>$last["subc".$i]));

                echo $c["name"].'<br/>'."(".$c["pname"].")";
                print_avaible_categories($n+1,$c["pname"]);

                echo '</li>';
            }
            /*foreach($collection->find(array("pname"=>$pname)) as $c) {
                echo '<li>';

                echo $c["name"]."(".$c["pname"].")";
                print_avaible_categories($n+1,$c["pname"]);

                echo '</li>';
            }*/
            echo '</ul>';
        }
    }
    print_avaible_categories(1,"pname");
    echo '
		</ul>
		</section>
		
		<section id="admin_add_new_word_to_dictionary">
		<h2>Добавить слово в словарь</h2>
		<form action="personal_page.php" method="POST" id="form_for_category">
			<input class="field" required name="name" placeholder="название" />
			<input class="field"  required name="definition" placeholder="определение" />
			
            <input name="sending" value="1" readonly style="display: none">
            <input name="dictionary" value="1" readonly style="display: none"><br/>
            <input class="btn" type="submit" value="отправить">
		</form>
		</section>
		<br>
		<section id="admin_a_to_add_quiz">
		<h3>Добавить викторину</h3>
	<a class="btn-arr" href="addquiz.php">Добавить  <i class="arrow"></i></a>
		</section>
		';
}
?>
<br/>
<section id="addnewrecipe">
    <h3>Добавить новый рецепт</h3>
    <a class="btn-arr" href="addrecipe.php">Добавить  <i class="arrow"></i></a>
</section>
<br/>
<section id="list_of_all_late_recipes">
    <h2>Доступные другим пользователям рецепты</h2>
    <?php

    $collection=$db->category1;
    $categories=$collection->find();
    foreach($categories as $c) {
        echo '<h4>'.$c["name"].'</h4>';
        echo '<ul><li  class="recipe_list">';

        foreach ($accepted_recipes[$c["pname"]] as $recipe) {

              $collection1='recipe'.$recipe['category_pname'];
              $collection=$db->$collection1;
              $user_id=$collection->findOne(array('name'=>$recipe['name']));

              $collection=$db->user;
               $id=$collection->findOne(array(
                "_id"=>new MongoDB\BSON\ObjectId($user_id['user_id'])
               ));
             if($_SESSION['name']==$id['name'])
            {
                if($recipe["category_pname"]!=$c["pname"]) continue;
                echo '<div class="elem_recipe">';
                echo  '  <a href="viewrecipe.php?recipe='.$recipe['_id'].'&namerecipe='.$recipe['name'].'">'.$recipe['name'].' </a>';
                echo '<br/>';
                echo '</div>';
            }

        }

        echo '</li></ul>';
    }
    ?>
    <br/>
    <h2>Изменённые и не провереные ещё рецепты</h2>
    <ul>
        <?php
        foreach ($changed_recipes as $recipe) {
            echo ' <li class="recipe_list">';
            echo '<div class="elem_recipe">';
            echo '  <a href="viewrecipe.php?recipe='.$recipe['_id'].'&namerec='.$recipe['name'].'&changed=1">'.$recipe['name'].' </a><br/>';
            echo 'Описание: ' . $recipe['description'] . '<br/>';
            echo '<br/>';
            echo '</div>';
            echo'</li>';
        }
        ?>
    </ul>  <br/>
    <h2>Заблокированные рецепты </h2>
    <ul>
        <?php
        foreach ($banned_recipes as $recipe) {
            echo ' <li class="recipe_list">';
            echo '<div class="elem_recipe">';
            echo  '  <a href="viewrecipe.php?_id='.$recipe['_id'].'&recipe='.$recipe['_id'].'&namerec='.$recipe['name'].'&banned=1">'.$recipe['name'].' </a><br/>';
            echo 'Описание: ' . $recipe['description'] . '<br/>';
            echo '<br/>';
            echo '<p color="red">сообщение: '.$recipe["message"].'</p>';
            echo '</div></li>';
        }
        ?>
    </ul>

    <?php /*<ul>
            <?php
            if(empty($recipes)) echo "<li>рецептов добавленных этим пользователем небыло найдено</li>";



            /*function short($s) {
                if(strlen($s)>500) {
                    return substr($s,0,500)."...";
                }
                return $s;
            }
            foreach($recipes as $i) {
                echo "<li>";
                echo "<h6>".$i["name"]."</h6>";
                echo "<p>".short($i["description"])."</p>";
                echo "</li>";
            }*
            ?>
        </ul>*/?>
</section>
</body>
</html>
