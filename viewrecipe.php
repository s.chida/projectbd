<?php
require 'vendor/autoload.php';
session_start();
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');


$db = $client->recipes;

$collection1='recipe'.$_SESSION['category'][0];
$collection=$db->$collection1;

$data=$collection->findOne(array(
        'name'=>$_GET['namerecipe']
)
);
$collection=$db->user;
$name_user=$collection->findOne(array(
    '_id'=>new MongoDB\BSON\ObjectId($data['user_id'])
));

$counting=$data['count_ing'];
$countrev=$data['count_reviews'];
$count=$countrev+1;
if(!empty($_POST['review']))
{
     $review=array(
            'u'.$count=>$_SESSION['name'],
            'r'.$count=>$_POST['review']

        );
    $collection1='recipe'.$_SESSION['category'][0];
    $collection2=$db->$collection1;
        $collection2->updateOne(
           array('name'=>$data['name']),
            array('$set'=>array(
                'u'.$count=>$_SESSION['name'],
                'r'.$count=>$_POST['review'],
                'count_reviews'=>$count
            )),
        );

header('location:viewrecipe.php?recipe='.$_GET['recipe'].'&category='.$_GET['category'].'&namerecipe='.$_GET['namerecipe']);
}

if($_GET['banned']==1 )
{
    $db=$client->recipes;
    $collection=$db->banned_recipes;

    $data=$collection->findOne(array(
       'name'=>$_GET['namerec']
    ));

    $collection=$db->user;
    $name_user=$collection->findOne(array(
        '_id'=>new MongoDB\BSON\ObjectId($data['user_id'])
    ));
    $counting=$data['count_ing'];
    $countrev=$data['count_reviews'];
}

if($_GET['check']==1 && !empty($_SESSION['admin']))
{
    $db=$client->recipes;
    $collection=$db->changed_recipes;

    $data=$collection->findOne(array(
        "_id"=>new MongoDB\BSON\ObjectId($_GET["recipe"])
    ));

    $collection=$db->user;
    $name_user=$collection->findOne(array(
        '_id'=>new MongoDB\BSON\ObjectId($data['user_id'])
    ));
    $counting=$data['count_ing'];
    $countrev=$data['count_reviews'];
    if(isset($_POST['buttonApprove']))
    {
        $db = $client->recipes;
        $collection1='recipe'.$data['subc1'];
        $collection=$db->$collection1;
        $newrecipe=array();
       // $newrecipe['_id']=   $_GET['recipe'];
        $newrecipe['name']=$data['name'];
        $newrecipe['user_id']=$data['user_id'];
        $newrecipe['count_ing']=$data['count_ing'];
        for($i=1;$i<=$data['count_ing'];$i++) {
            $newrecipe['ing'.$i]=   $data['ing'.$i];
            $newrecipe['c'.$i]=$data['c'.$i];
        }
        $newrecipe['description']=$data['description'];
        $newrecipe['count_reviews']=$data['count_reviews'];
        for($i=1;$i<=$data["count_reviews"];$i++) {
            $newrecipe["r".$i]=$data["r".$i];
            $newrecipe["u".$i]=$data["u".$i];
        }

        $newrecipe["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);

        $newrecipe["ldescription"]=$data["ldescription"];
        $newrecipe["subc_n"]=$data['subc_n'];
        for($i=1;$i<=$data['subc_n'];$i++) {
            $newrecipe["subc".($i)]=$data['subc'.($i)];
        }
        $collection->insertOne($newrecipe);

        $db=$client->recipes;
        $col=$db->changed_recipes;
        $col->deleteOne(array(
            'name'=>$data['name']
        ));
        $col=$db->banned_recipes;
        $col->deleteOne(array(
            'name'=>$data['name']
        ));
        header('location:personal_page.php');

    }
    if(isset($_POST['buttonBan']))
    {
        $db = $client->recipes;
        $collection=$db->banned_recipes;

        $newrecipe=array();
        //$newrecipe['_id']=$_GET['recipe'];
        $newrecipe['name']=$data['name'];
        $newrecipe['user_id']=$data['user_id'];
        $newrecipe['count_ing']=$data['count_ing'];
        for($i=1;$i<=$data['count_ing'];$i++) {
            $newrecipe['ing'.$i]=   $data['ing'.$i];
            $newrecipe['c'.$i]=$data['c'.$i];
        }
        $newrecipe['description']=$data['description'];
        $newrecipe['count_reviews']=$data['count_reviews'];
        for($i=1;$i<=$data["count_reviews"];$i++) {
            $newrecipe["r".$i]=$data["r".$i];
            $newrecipe["u".$i]=$data["u".$i];
        }

        $newrecipe["added"]=$data["added"];
        $newrecipe["banned"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["ldescription"]=$data["ldescription"];
        $newrecipe['message']=$_POST['message'];
        $newrecipe["subc_n"]=$data['subc_n'];
        for($i=1;$i<=$data['subc_n'];$i++) {
            $newrecipe["subc".($i)]=$data['subc'.($i)];
        }

        $collection->insertOne($newrecipe);
        $db=$client->recipes;
        $col=$db->changed_recipes;
        $col->deleteOne(array(
            'name'=>$data['name']
        ));
        header('location:personal_page.php');
    }
    /*$name_category=$collection->findOne(array(
        '_id'=> new MongoDB\BSON\ObjectId($_GET['category'])
    ));
    $db = $client->recipes;
    $collection=$db->changed_recipes;
    $data=$collection->findOne(array(
       '_id'=> new MongoDB\BSON\ObjectId($_GET['recipe'])
    ));
    $collection=$db->user;
    $name_user=$collection->findOne(array(
        '_id'=>new MongoDB\BSON\ObjectId($data['user_id'])
    ));
    $counting=$data['count_ing'];
    $countrev=$data['count_reviews'];

    if(isset($_POST['buttonApprove']))
    {
        $db = $client->recipes;
        $collection1='recipes'.$name_category['pname'];
        $collection=$db->$collection1;
        $newrecipe=array();
        $newrecipe['_id']=$_GET['recipe'];
        $newrecipe['name']=$data['name'];
        $newrecipe['user_id']=$data['user_id'];
        $newrecipe['count_ing']=$data['count_ing'];
        for($i=1;$i<=$data['count_ing'];$i++) {
            $newrecipe['ing'.$i]=   $data['ing'.$i];
            $newrecipe['c'.$i]=$data['c'.$i];
        }
        $newrecipe['description']=$data['description'];
        $newrecipe['count_reviews']=$data['count_reviews'];
        for($i=1;$i<=$data["count_reviews"];$i++) {
            $newrecipe["r".$i]=$data["r".$i];
            $newrecipe["u".$i]=$data["u".$i];
        }

        $newrecipe["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["category"]=$_GET['category'];
        $newrecipe["ldescription"]=$data["ldescription"];
        $collection->insertOne($newrecipe);

        $db=$client->recipes;
        $col=$db->changed_recipes;
        $col->deleteOne(array(
            'name'=>$data['name']
        ));
        header('location:personal_page.php');

    }

    if(isset($_POST['buttonBan']))
    {
        $db = $client->recipes;
        $collection=$db->banned_recipes;

        $newrecipe=array();
        $newrecipe['_id']=$_GET['recipe'];
        $newrecipe['name']=$data['name'];
        $newrecipe['user_id']=$data['user_id'];
        $newrecipe['count_ing']=$data['count_ing'];
        for($i=1;$i<=$data['count_ing'];$i++) {
            $newrecipe['ing'.$i]=   $data['ing'.$i];
            $newrecipe['c'.$i]=$data['c'.$i];
        }
        $newrecipe['description']=$data['description'];
        $newrecipe['count_reviews']=$data['count_reviews'];
        for($i=1;$i<=$data["count_reviews"];$i++) {
            $newrecipe["r".$i]=$data["r".$i];
            $newrecipe["u".$i]=$data["u".$i];
        }

        $newrecipe["added"]=$data["added"];
        $newrecipe["banned"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["category"]=$_GET['category'];
        $newrecipe["ldescription"]=$data["ldescription"];
        $newrecipe['message']=$_POST['message'];
        $collection->insertOne($newrecipe);
        $db=$client->recipes;
        $col=$db->changed_recipes;
        $col->deleteOne(array(
            'name'=>$data['name']
        ));
        header('location:personal_page.php');
    }*/

}
if($_GET['changed']==1)
{
    $db=$client->recipes;
    $collection=$db->changed_recipes;

    $data=$collection->findOne(array(
        '_id'=>new MongoDB\BSON\ObjectId($_GET['recipe'])
    ));

    $collection=$db->user;
    $name_user=$collection->findOne(array(
        '_id'=>new MongoDB\BSON\ObjectId($data['user_id'])
    ));
    $counting=$data['count_ing'];
    $countrev=$data['count_reviews'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head xmlns="http://www.w3.org/1999/html">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="viewrcipe.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Рецепты</title>
</head>
<body>
<header>
    <?php require "header.php"; ?>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <div class="recipe">

            <h1><?php print($data['name']).'<br/>'?> </h1>
            <?php

            echo ' <h3>ИНСТРУКЦИЯ ПРИГОТОВЛЕНИЯ</h3><br>' . $data['ldescription'] . '<br/><br/>';
            echo' <p style="text-decoration: underline">Автор рецепта</p><a>'.$name_user['name'].'</a>';
           // $date_created = $data['added'];
           // $iso_date = $date_created->toDateTime()->format("Y-m-d H:i:s");
            //echo ' <p style="text-decoration: underline">Дата добавления</p><a> ' . $iso_date . '<a>мин.</a><br/><br/>';

   ?>
                <h3>ИНГРЕДИЕНТЫ</h3>

                    <tr>
                        <td><?php for($i=1;$i<=$counting;$i++) {
                                echo $data['ing'.$i].'<a>: <a/>';
                                echo $data['c'.$i].'<br/>';
                            }
                            ?>
                        </td>
                    </tr><br/>

                <p class="line"><span>ОТЗЫВЫ</span></p>
                <dl class="reviews">
            <?php
            for($i=$countrev;$i>=1;$i--) {
                echo '<dt>'.$data['u'.$i].'</dt>';
                echo '<dd>'.$data['r'.$i].'</dd>';
            }
            ?>
                </dl>
            <br/>



            <?php if (!empty($_SESSION['name']))
                echo '  
           <br/>
            <form action="" method="post" autocomplete="off">
                <textarea name="review"  placeholder="Добавьте комментарий" type="text"></textarea>
                
            </label><br/>
                <input class="btn" type="submit" name="buttonReview" value="Отправить" />
            </form>
                ';

            ?>
       <?php
      if(!empty($_SESSION["admin"]) && $_GET['check']==1)
      {
          echo ' <p class="line"><span>Модерация рецептов</span></p>';
          echo' <div class="formCheck">
       <form action="" method="post" name="" >
       <input type="submit" name="buttonApprove" value="Одобрить" />
       <a> или </a>
        <input type="submit" name="buttonBan" value="Заблокировать" /><br/>
      
       <textarea name="message" type="text" placeholder="В случае блокировки рецепта оставьте комментарий"></textarea>
</form>
</div><br/>';
      }

       if (($_SESSION['name']==$name_user['name']) && $_GET['check']!=1)
           echo ' <a  class="change" href="addrecipe.php?_id=' . $data['_id'] . '&namereci=' . $data['name'] . '"> РЕДАКТИРОВАТЬ РЕЦЕПТ</a><br/>';


       ?>
        </div>
        </div>
    </div>
</div>
</body>
