<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
$db = $client->recipes;

session_start();
$signin=true;
if(empty($_SESSION["name"])) {
    $signin=false;
} else if(!empty($_POST["sending"])) {
    //подключение к mongodb и добавление рецепта
    //c параметрами $recipe пользователем с именем $_SESSION["name"]
    // c пустым списком коментариев когда $_GET['_id'] отсутствует
    // и обновление данных при $_GET["_id"]
    if(empty($_GET["_id"])) {
        $collection=$db->user;
        $user_id=$collection->findOne(array('name'=>$_SESSION["name"]));
        $user_id=$user_id["_id"];

        //$collection=$db->category;
        //$category_id=$collection->findOne(array("pname"=>$_POST["category_pname"]));
        //$category_id=$category_id["_id"];

        $collection=$db->history_of_recipes;
        $newrecipe=array();
        $newrecipe["name"]=$_POST["name"];
        $newrecipe["user_id"]=$user_id;
        $newrecipe["count_ing"]=count($_POST["ing"]);
        for($i=1;$i<=count($_POST["ing"]);$i++) {
            $newrecipe["ing".$i]=$_POST["ing"][$i-1];
            $newrecipe["c".$i]=$_POST["c"][$i-1];
        }
        $newrecipe["description"]=$_POST["description"];
        $newrecipe["count_reviews"]=0;
        $newrecipe["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["date_of_manipulation"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["type_of_manipulation"]="add";
        //$newrecipe["category"]=$category_id;

        $newrecipe["subc_n"]=count($_POST["categories_pname"]);
        for($i=0;$i<count($_POST["categories_pname"]);$i++) {
            $newrecipe["subc".($i+1)]=$_POST["categories_pname"][$i];
        }

        $newrecipe["ldescription"]=$_POST["ldescription"];
        $collection->insertOne($newrecipe);

        $collection=$db->changed_recipes;
        $newrecipe=array();
        $newrecipe["name"]=$_POST["name"];
        $newrecipe["user_id"]=$user_id;
        $newrecipe["count_ing"]=count($_POST["ing"]);
        for($i=1;$i<=count($_POST["ing"]);$i++) {
            $newrecipe["ing".$i]=$_POST["ing"][$i-1];
            $newrecipe["c".$i]=$_POST["c"][$i-1];
        }
        $newrecipe["description"]=$_POST["description"];
        $newrecipe["count_reviews"]=0;
        $newrecipe["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["changed"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        //$newrecipe["category"]=$category_id;

        $newrecipe["subc_n"]=count($_POST["categories_pname"]);
        for($i=0;$i<count($_POST["categories_pname"]);$i++) {
            $newrecipe["subc".($i+1)]=$_POST["categories_pname"][$i];
        }

        $newrecipe["ldescription"]=$_POST["ldescription"];
        $collection->insertOne($newrecipe);
    } else {
        /*$category='recipe'.$_POST["pname"];
        $collection=$db->$category;
        $recipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));

        $collection=$db->user;
        $user_id=$collection->findOne(array('name'=>$_SESSION["name"]));
        $user_id=$user_id["_id"];

        $collection=$db->category;
        $category_id=$collection->findOne(array("pname"=>$recipe["pname"]));
        $category_id=$category_id["_id"];*/
        $category='recipe'.$_POST["categories_pname"];
        $collection=$db->$category;
        $recipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
        if(empty($recipe["name"])) {
            $collection=$db->changed_recipes;
            $recipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
        }
        if(empty($recipe["name"])) {
            $collection=$db->banned_recipes;
            $recipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
        }
        $collection->deleteOne(array("_id"=>$recipe["_id"]));

        $collection=$db->user;
        $user_id=$collection->findOne(array('name'=>$_SESSION["name"]));
        $user_id=$user_id["_id"];
        //$category_id=$category_id["_id"];

        $collection=$db->history_of_recipes;
        $newrecipe=array();
        $newrecipe["name"]=$recipe["name"];
        $newrecipe["user_id"]=$user_id;
        $newrecipe["count_ing"]=$recipe["count_ing"];
        for($i=1;$i<=$recipe["count_ing"];$i++) {
            $newrecipe["ing".$i]=$recipe["ing"][$i-1];
            $newrecipe["c".$i]=$recipe["c"][$i-1];
        }
        $newrecipe["description"]=$recipe["description"];
        $newrecipe["count_reviews"]=$recipe["count_reviews"];
        for($i=1;$i<=$recipe["count_reviews"];$i++) {
            $newrecipe["r".$i]=$recipe["r".$i];
            $newrecipe["u".$i]=$recipe["u".$i];
        }
        $newrecipe["added"]=$recipe["added"];
        $newrecipe["date_of_manipulation"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        $newrecipe["type_of_manipulation"]="change";
        //$newrecipe["category"]=$category_id;

        $newrecipe["subc_n"]=$recipe['subc_n'];
        for($i=1;$i<=$recipe['subc_n'];$i++) {
            $newrecipe["subc".$i]=$recipe['subc'.$i];
        }

        $newrecipe["ldescription"]=$recipe["ldescription"];
        $collection->insertOne($newrecipe);

        $collection=$db->changed_recipes;
        $newrecipe=array();
        $newrecipe["name"]=$_POST["name"];
        $newrecipe["user_id"]=$user_id;
        $newrecipe["count_ing"]=count($_POST["ing"]);
        for($i=1;$i<=count($_POST["ing"]);$i++) {
            $newrecipe["ing".$i]=$_POST["ing"][$i-1];
            $newrecipe["c".$i]=$_POST["c"][$i-1];
        }
        $newrecipe["description"]=$_POST["description"];
        $newrecipe["count_reviews"]=$recipe["count_reviews"];
        for($i=1;$i<=$recipe["count_reviews"];$i++) {
            $newrecipe["r".$i]=$recipe["r".$i];
            $newrecipe["u".$i]=$recipe["u".$i];
        }
        $newrecipe["added"]=$recipe["added"];
        $newrecipe["changed"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
        //$newrecipe["category"]=$category_id;

        $newrecipe["subc_n"]=$recipe['subc_n'];
        for($i=1;$i<=$recipe['subc_n'];$i++) {
            $newrecipe["subc".$i]=$recipe['subc'.$i];
        }

        $newrecipe["ldescription"]=$_POST["ldescription"];
        $collection->insertOne($newrecipe);

        //$category='recipes'.$_POST["pname"];
        //$collection->$db->$category;
        //$collection->deleteOne(array("_id"=>$recipe["_id"]));
    }

    header('location: index.php');
    exit();
} else if(!empty($_GET["_id"])) {
    //подключение к базе данных и поиск рецепта с _id=$_GET["_id"]
    //в каждой категории пока не найден результат
    $collection = $db->category1;
    $cursor = $collection->find();
    $dbrecipe=array();
    foreach($cursor as $c) {
        $category='recipe'.$c["pname"];
        $collection=$db->$category;

        $dbrecipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
        if(empty($dbrecipe["name"])) continue;

        //$category_pname=$c["pname"];
        break;
    }
    if(empty($dbrecipe["name"])) {
        $collection = $db->changed_recipes;
        $dbrecipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
    }
    if(empty($dbrecipe["name"])) {
        $collection = $db->banned_recipes;
        //$dbrecipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
        $dbrecipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));

    }
    //$collection = $db->recipespasta;
    //$dbrecipe=$collection->findOne(array("_id"=>new MongoDB\BSON\ObjectId($_GET["_id"])));
    //$dbrecipe=$collection->findOne(array("name"=>"Паста со свежими овощами"));

    $recipe=array();
    //$recipe["category_pname"]=$category_pname;
    $recipe["categories_pname"]=array();
    for($i=0;$i<$dbrecipe["subc_n"];$i++) {
        $recipe["categories_pname"][$i]=$dbrecipe["subc".($i+1)];
    }
    $recipe["name"]=$dbrecipe["name"];
    $recipe["ing"]=array();
    $recipe["c"]=array();
    for($i=1;$i<=$dbrecipe["count_ing"];$i++) {
        $recipe["ing"][]=$dbrecipe["ing".$i];
        $recipe["c"][]=$dbrecipe["c".$i];
    }
    $recipe["description"]=$dbrecipe["description"];
    $recipe["ldescription"]=$dbrecipe["ldescription"];
} else {
    $recipe=array(
        "ing"=>array()
    );
}

/*$collection = $db->category;
$cursor = $collection->find();
$categories=array();
foreach($cursor as $c) {
    $categories[]=array("name"=>$c["name"],
        "pname"=>$c["pname"],
        "description"=>$c["description"]
    );
}*/



$categories=array();
function build_catefories(&$categories) {
    $c=array();
    $client = new MongoDB\Client(
        'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
    $db = $client->recipes;

    $collection=$db->category1;
    foreach($collection->find() as $r) {
        $c[0]=$r;
        $categories[]=build_categories_pr($c);
    }
}
function build_categories_pr(&$c) {
    $categories=array();
    $client = new MongoDB\Client(
        'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
    $db = $client->recipes;
    $categories["name"]=$c[count($c)-1]["name"];
    $categories["pname"]=$c[count($c)-1]["pname"];

    if($c[count($c)-1]["subc_n"]==0) return $categories;

    $cat="category".(count($c)+1);
    $collection=$db->$cat;

    $categories["subc_n"]=$c[count($c)-1]["subc_n"];
    $categories["subc"]=array();
    for($i=1;$i<=$c[count($c)-1]["subc_n"];$i++) {
        $r=$collection->findOne(array("pname"=>$c[count($c)-1]["subc".$i]));
        $c[count($c)]=$r;

        $categories["subc"][]=build_categories_pr($c);

        array_pop($c);
    }

    return $categories;
}
build_catefories($categories);
//print_r($categories);

//print the three of the categories for javascript
function print_categories(&$categories) {
    echo 'let categories=';
    print_categories_pr($categories);
    echo ';';
}
function print_categories_pr($categories) {
    echo '[';
    for($i=0;$i<count($categories);$i++) {
        echo '{';
        echo 'name:"'.$categories[$i]["name"].'",';
        echo 'pname:"'.$categories[$i]["pname"].'"';
        if(!empty($categories[$i]["subc"])&&count($categories[$i]["subc"])!=0) {
            echo ',';
            echo 'subc_n:'.count($categories[$i]["subc"]).',';
            echo 'subc:';
            print_categories_pr($categories[$i]["subc"]);
        }
        echo '}';
        if(count($categories)-1!=$i) echo ',';
    }
    echo ']';
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="style_addrecipe.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Рецепты</title>
    <script>
        function addIngridient() {
            let vul=document.getElementById("list_of_avaible_ingridients");
            let vli=vul.getElementsByTagName("li");

            let newingridient=document.createElement("li");
            newingridient.innerHTML='<label><div>название </div>' +
                '<input required name="ing['+vli.length+']" placeholder="название" ' +
                '</label><label><div>количество </div>'+
                '<input required name="c['+vli.length+']" placeholder="количество"'+
                '</label></li>';
            list_of_avaible_ingridients.append(newingridient);

        }
        function deleteLastIngridient() {
            let vul=document.getElementById("list_of_avaible_ingridients");
            let vli=vul.getElementsByTagName("li");
            if(vli.length==0) return;
            let vlastli=vli[vli.length-1];
            vlastli.remove();
        }



        <?php print_categories($categories); ?>

        function change_categories(n) {
            //let a=document.getElementsByTagName('p')[0];
            //a.innerHTML=a.innerHTML+n;

            let selects=choosecategory.getElementsByTagName('select');
            let l=selects.length;
            for(let i=n+1;i<=l;i++) {
                selects[n].remove();
            }

            let cat=JSON.parse(JSON.stringify(categories));
            for(let i=0;i<selects.length;i++) {
                let j;
                for(j=0;j<cat.length;j++) if(cat[j].pname==selects[i].value) break;
                cat=cat[j].subc;
            }

            while(cat!=undefined) {
                /*let newstring='<select id="c'+(n+1)+'" name="categories_pname['+n+']" required onchange="change_categories('+(n+1)+')">\n';
                for(let i=0;i<cat.length;i++) {
                    newstring=newstring+'<option value="'+cat[i]['pname']+'">'+cat[i]['name']+'</option>\n';
                }
                newstring=newstring+'</select>';

                choosecategory.innerHTML=choosecategory.innerHTML+newstring;*/
                let newop=document.createElement('select');
                newop.name='categories_pname['+n+']';
                let m=n+1;
                newop.onchange=function() { change_categories(m); };
                newop.required=true;
                let newopHTML='';
                for(let i=0;i<cat.length;i++) {
                    newopHTML=newopHTML+'<option value="'+cat[i]['pname']+'">'+cat[i]['name']+'</option>\n';
                }
                newop.innerHTML=newopHTML;

                choosecategory.append(newop);

                n++;
                cat=cat[0].subc;
            }
            //console.log(choosecategory.innerHTML);
        }
    </script>
</head>
<body>
<header>
    <?php require "header.php"; ?>
</header>

<?php
if($signin==false) {
    echo "<p>чтобы воспользоваться этой стриницей 
                необхидимо зайти как зарегистрированный 
                пользователь</p></body></html>";
    exit();
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-7">
            <div id="form">
                <div class="outer">
                    <div class="middle">
                        <div class="inner">
                            <div class="login">

                                <form action="addrecipe.php<?php if(!empty($_GET["_id"])) echo "?_id=".$_GET["_id"];?>" method="POST">
                                    <h3>
                                        <?php
                                        if(!empty($_GET["_id"])) echo "<h1>Редактирование рецепта</h1>";
                                        else echo "<h1>Создание рецепта</h1>";
                                        ?>
                                    </h3>

                                    <label>
                                        <div>Название рецепта</div><br />
                                        <input required name="name" placeholder="название"
                                            <?php
                                            if(!empty($recipe["name"])) echo ' value="'.$recipe["name"].'" ';
                                            ?>
                                        />
                                    </label>

                                    <h6>категория</h6>
                                    <?php
                                    /*
                                    if(empty($_GET["_id"])) {
                                        echo '<select name="category_pname" required>';
                                        foreach($categories as $c) {
                                            echo '<option value="'.$c["pname"].'" ';
                                            if(!empty($recipe["category_pname"])&&$recipe["category_pname"]==$c["pname"]) echo ' checked ';
                                            echo '>'.$c["name"].'</option>';
                                        }
                                        echo '</select>';
                                    } else {
                                        echo '<select name="category_pname" required>';
                                        foreach($categories as $c) {
                                            if($recipe["category_pname"]==$c["pname"]) continue;

                                            echo '<option value="'.$c["pname"].'" ';
                                            echo ' checked ';
                                            echo '>'.$c["name"].'</option>';

                                            break;
                                        }
                                        echo '</select>';
                                    }
                                    */
                                    if(!empty($_GET["_id"])) {
                                        echo '<div id="choosecategory">';
                                        $cat=$categories;
                                        for($i=0;$i<count($recipe["categories_pname"]);$i++) {
                                            echo '<select name="categories_pname['.$i.']" required onchange="change_categories('.($i+1).')">';
                                            foreach($cat as $c) {
                                                echo '<option value="'.$c["pname"].'"';
                                                if($c["pname"]==$recipe["categories_pname"][$i]) echo ' checked';
                                                echo '>'.$c["name"].'</option>';
                                            }


                                            $j=0;
                                            while($cat[$j]["pname"]!=$recipe["categories_pname"][$i]) ++$j;
                                            $cat=$cat[$j]["subc"];
                                            echo '</select>';
                                        }
                                        echo '</div>';
                                    } else {
                                        echo '
												<div id="choosecategory">
												<select name="categories_pname[0]" required onchange="change_categories(1)">
													<option value="pasta">паста</option>
													<option value="desert">десерт</option>
													<option value="other">другое</option>
												</select>
												</div>
												<script>change_categories(0);</script>';
                                    }
                                    ?>

                                    <div>ингридиенты</div>
                                    <ul id="list_of_avaible_ingridients">
                                        <?php
                                        $k=0;
                                        foreach($recipe["ing"] as $i) {
                                            echo '<li>';
                                            echo '<label>
                                    <div>название </div>
                                    <input required name="ing['.$k.']" placeholder="название"
                                        value="'.$i.'"
                                    </label>';
                                            echo '<label>
                                    <div>количество </div>
                                    <input required name="c['.$k.']" placeholder="количество"
                                        value="'.$recipe["c"][$k].'"
                                    </label>';
                                            echo '</li>';
                                            $k++;
                                        }
                                        ?>
                                    </ul>
                                    <button type="button" onclick="addIngridient()" >добавить ингридиент</button>
                                    <button type="button" onclick="deleteLastIngridient()" >удалить последний ингридиент</button>

                                    <label>
                                        <div>описание</div>
                                        <textarea required name="ldescription" placeholder="введите описание">
                            <?php if(!empty($recipe["ldescription"])) echo $recipe['ldescription'];?>
                        </textarea>
                                    </label>
                                    <br />

                                    <label>
                                        <div>краткое описание для отображения в списке рецептов</div>
                                        <textarea required name="description" placeholder="введите описание">
                            <?php if(!empty($recipe["description"])) echo $recipe['description'];?>
                        </textarea>
                                    </label>
                                    <br />

                                    <input name="sending" value="1" readonly style="display: none">
                                    <input type="submit" value="отправить">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
