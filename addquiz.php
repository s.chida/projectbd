<?php
require 'vendor/autoload.php';
$client = new MongoDB\Client(
    'mongodb+srv://team1:Ilovebd2021@cluster0.upmi2.mongodb.net/recipes?retryWrites=true&w=majority');
$db = $client->recipes;

session_start();
$signin=true;
if(empty($_SESSION["name"])) {
    $signin=false;
} else if(empty($_SESSION["admin"])) {
	$signin=false;
} else if(!empty($_POST["sending"])) {
	$quiz=array();
	$quiz["name"]=$_POST["name"];
	$quiz["count_q"]=count($_POST["q"]);
	$quiz["added"]=new \MongoDB\BSON\UTCDateTime(time()*1000);
	$collection=$db->user;
	$user_id=$collection->findOne(array("name"=>$_SESSION["name"]));
	$user_id=$user_id["_id"];
	$quiz["admin"]=$user_id;
	
	for($i=0;$i<count($_POST["q"]);$i++) {
		$ri=array();
		$ri["count_r"]=count($_POST["v"][$i]);
		for($j=0;$j<count($_POST["v"][$i]);$j++) {
			$ri["v".($j+1)]=$_POST["v"][$i][$j];
		}
		$ri["correct_r"]=$_POST["correct_r"][$i];
		
		$collection=$db->response_quizzes;
		$lr=$collection->findOne($ri);
		if(empty($rl)) {
			$collection->insertOne($ri);
			$ri=$collection->findOne($ri);
		} else {
			$ri=$lr;
		}
		
		$quiz["q".($i+1)]=$_POST["q"][$i];
		$quiz["r".($i+1)]=$ri["_id"];
	}
	$collection=$db->quizzes;
	$collection->insertOne($quiz);
	
	header('location: personal_page.php');
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Рецепты</title>
	<script>
        function addResponce(qi) {
            let vul=document.getElementById("questions");
            let vli=vul.getElementsByClassName("ulli");
            let li=vli[qi];
			
			let lr=li.getElementsByTagName("ol");
			lr=lr[0];
			lrli=lr.getElementsByTagName("li");
			let newresponse=document.createElement("li");
			let rn=lrli.length;
            newresponse.innerHTML='<input required name="v['+qi+']['+rn+']" placeholder="ответ"/>';
            lr.append(newresponse);
        }
        function deleteLastResponce(qi) {
            let vul=document.getElementById("questions");
            let vli=vul.getElementsByClassName("ulli");
            let li=vli[qi];
			
			let lr=li.getElementsByTagName("ol");
			lr=lr[0];
			lilr=lr.getElementsByTagName("li");
			if(lilr.length==1) return;
			let lastr=lilr[lilr.length-1];
            lastr.remove();li
        }
		
        function addQuestion() {
            let vul=document.getElementById("questions");
            let vli=vul.getElementsByClassName("ulli");
			
			let qn=vli.length;
            let newquestion=document.createElement("li");
            newquestion.innerHTML='<div class="question" id="q'+qn+'">'+
							'<input required name="q['+qn+']" placeholder="название воароса"/>'+
							'<ol id="r'+qn+'"><li><input required name="v['+qn+'][0]" placeholder="ответ"/></li>'+
							'</ol>'+
							'<label>номер правильного ответа <input required name="correct_r['+qn+']" placeholder="номер правильного ответа"></label><br />'+
							'<button type="button" onclick="addResponce('+qn+')" >добавить ответ</button>'+
							'<button type="button" onclick="deleteLastResponce('+qn+')" >удалить последний ответ</button>'+
						'</div>';
			newquestion.className="ulli";
            questions.append(newquestion);
        }
        function deleteLastQuestion() {
            let vul=document.getElementById("questions");
            let vli=vul.getElementsByClassName("ulli");
            if(vli.length==1) return;
            let vlastli=vli[vli.length-1];
            vlastli.remove();
        }
    </script>
    <style>
        body {
            background-color: black;
            font-family: Ebrima;
        }
        .row {
            background-color: beige;
            padding: 30px;
            border: 10px solid mediumseagreen;
            width: 660px;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
        }
        input {
            border: 0;
            padding: 8px;
            margin: 8px;
            width: 200px;
        }
        .snd {
            position: relative;
            overflow: hidden;
            z-index: 1;
            padding: 8px 30px;
            margin: 20px 0 20px 0;
            font-family: 'Montserrat Alternates', sans-serif;
            font-weight: 600;
            line-height: 30px;
            color: white;
            font-size: 15px;
            text-transform: uppercase;
            background: mediumseagreen;
            border-width: 0;
            box-shadow: 5px 5px 0 #532831;
            outline: none;
            cursor: pointer;
            transition: 1.5s;
            left: 50%;
            transform: translate(-50%, 0);
        }
        .snd:before, .snd:after {
            content: "";
            position: absolute;
            height: 200px;
            left: -50%;
            margin-top: -100px;
            top: 50%;
            width: 200px;
            border-radius: 50%;
            opacity: 0.3;
            z-index: -1;
            transform: scale(0);
        }
        .snd:before {
            background: #ffeede;
            transition: .8s ease-out;
        }
        .snd:after {
            transition: .4s ease-in .3s;
        }
        .snd:hover {
            color: #532831;
        }
        .snd:hover:before, .snd:hover:after {
            opacity: 1;
            transform: scale(4);
        }
        button {
            text-decoration: none;
            padding: 10px 20px;
            margin: 10px 10px 10px 0;
            border: 2px solid seagreen;
            border-radius: 8px;
            color: seagreen;
            background-color: transparent;
        }
        .ulli {
            background-color: aliceblue;
            margin: 0 0 10px -40px;
            padding: 20px;
            list-style: none;
        }
        h2 {
            margin: 20px 0;
            font-weight: bolder;
            text-align: center;
        }
        h5 {
            margin: 10px;
        }
        form {
            width: 500px;
            margin: 30px 20px;
        }
        #addquiz_form {
            padding: 40px;
        }
    </style>
</head>
<body>
    <header>
        <?php require "header.php"; ?>
    </header>

    <?php
    if($signin==false) {
		if(empty($_SESSION["name"])) {
			echo "<p>чтобы воспользоваться этой стриницей 
                необхидимо зайти как загирустрированных 
                пользователь</p></body></html>";
		} else {
			echo "<p>чтобы воспользоваться этой стриницей 
                необхидимо зайти как загирустрированных 
                пользователь являющийся аднимистратором</p></body></html>";
		}
        exit();
    }
    ?>
	
	<section id=addquiz_form">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
				<form action="addquiz.php" method="POST">
					<h2>Создание викторины</h2>
					
					<label>
						<h5>Название викторины</h5><br />
						<input required name="name" placeholder="название"/>
					</label>

                    <h5>Введите вопросы викторины:</h5>
					<ul id="questions">
						<li class="ulli"><div class="question" id="q0">
							<input required name="q[0]" placeholder="название вопроса"/>
							<ol>
								<li><input required name="v[0][0]" placeholder="ответ"/></li>
								<?php /*<li><input required name="v[0][1]" placeholder="ответ"/></li>
								<li><input required name="v[0][2]" placeholder="ответ"/></li>
								<li><input required name="v[0][3]" placeholder="ответ"/></li>*/?>
							</ol>

                                <h6>Номер правильного ответа </h6>
                                <input required name="correct_r[0]" placeholder="номер правильного ответа">
                            <br/>
							<button type="button" onclick="addResponce(0)" >Добавить ответ</button>
							<button type="button" onclick="deleteLastResponce(0)" >Удалить последний ответ</button>
						</div></li>
						<?php /*<li class="ulli"><div class="question" id="q1">
							<input required name="q[1]" placeholder="название воароса"/>
							<ol>
								<li><input required name="v[1][0]" placeholder="ответ"/></li>
								<li><input required name="v[1][1]" placeholder="ответ"/></li>
								<li><input required name="v[1][2]" placeholder="ответ"/></li>
							</ol>
							<label>номер правильного ответа <input required name="correct_r[1]" placeholder="номер правильного ответа"></label><br />
							<button type="button" onclick="addResponce(1)" >добавить ответ</button>
							<button type="button" onclick="deleteLastResponce(1)" >удалить последний ответ</button>
						</div></li>*/?>
					</ul>
                    <button type="button" onclick="addQuestion()" >Добавить вопрос</button>
                    <button type="button" onclick="deleteLastQuestion()" >Удалить последний вопрос</button>
					
                    <br />
					
                    <input name="sending" value="1" readonly style="display: none">
                    <input type="submit" value="отправить">
				</form>
            </div>
        </div>
    </div>
	</section>
</body>
</html>